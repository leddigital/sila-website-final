@extends('layouts.default')
@section('content')

@section('social-tags')

    <meta property="og:title" content="Procurando por: '{{ $form }}' - Sila Decor">
    <meta property="og:description" content="Linha de móveis completa para sua casa">
    <meta property="og:image" content="{{ asset('images/logo.png') }}">
    <meta property="og:image:alt" content="{{ asset('images/logo.png') }}">

    <meta property="og:image:width" content="844"/>
    <meta property="og:image:height" content="394"/>
    <meta property="og:url" content="{{ route('nav.procurar', ['search' =>  $form ]) }}">


    <meta name="twitter:title" content="Procurando Por: '{{ $form }}' - Sila Decor">
    <meta name="twitter:description" content="Linha de móveis completa para sua casa">
    <meta name="twitter:image" content="{{ asset('images/logo.png') }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection


<!-- Content Scroll -->
<div id="content-scroll">


    <!-- Main -->
    <div id="main">



        <!-- Main Content -->
        <div id="main-content">
            <div id="main-page-content">

                @if (count($itens))

                <!-- Row -->
                <div class="vc_row row_padding_top small row_padding_bottom text-align-center">
                    <hr>
                    <h2 class="has-mask">Procurando Por: "{{ $form }}"</h2>
                </div>
                <!--/Row -->

                <!-- Row -->
                <div class="vc_row full has-animation" data-delay="100">
                    <!-- Collage -->
                    <div id="justified-grid">

                        @foreach ($itens as $item)

                        <!-- Collage item with pop-up -->
                        <div class="collage-thumb">
                            <a class="image-link" href="{{ asset('/produtos/'.$item->image) }}">
                                <img src="{{ asset('/produtos/alta/'.$item->image_alta) }}" alt="img" />
                                <div class="thumb-info">{{ $item->title }}</div>
                            </a>
                        </div>

                        @endforeach

                    </div>
                    <!--/Row -->

                    @else

                        <div class="vc_row row_padding_top small row_padding_bottom text-align-center">
                            <img src="{{ asset('not-found.png') }}" alt="">
                            <hr>
                            <h2 class="has-mask">Não foram encontrados produtos para "{{ $form }}".</h2>
                        </div>

                    @endif

                </div>
                <!--/Main Page Content -->

                <!-- Project Navigation -->
                <div id="project-nav">
                    <div class="next-project-wrap">
                        <div class="next-project-image">
                            <div class="next-project-image-bg" style="background-image:url(images/06hero.jpg)"></div>
                        </div>
                        <div class="next-project-title">
                            <div class="inner">
                                <div class="next-subtitle-info has-animation">Não encontrou o que procurava? Veja nossa
                                    linha de </div>
                                <div class="has-animation" data-delay="150">
                                    <a class="main-title next-ajax-link-project hide-ball" data-type="page-transition"
                                    href="{{ route('nav.categoria', ['categoria' => $nextCat->url]) }}">{{ $nextCat->title }}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--/Project Navigation -->


            </div>
            <!--/Main Content -->
        </div>
        <!--/Main -->

        <!-- Footer -->
        <footer class="hidden">
            <div id="footer-container">

                <div id="backtotop" class="button-wrap left-custom">
                    <div class="icon-wrap parallax-wrap">
                        <div class="button-icon parallax-element">
                            <i class="fa fa-angle-up"></i>
                        </div>
                    </div>
                    <div class="button-text"><span data-hover="Voltar ao topo">Voltar ao topo</span></div>
                </div>

                <div class="socials-wrap">
                    <div class="socials-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                    <div class="socials-text">Siga a Sila Decor</div>
                    <ul class="socials">

                        <li style="{{ isset($informations->facebook)!=""?'':'display:none;' }}" }}>
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->facebook }}" target="_blank">
                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>

                        <li style="{{ isset($informations->instagram)!=""?'':'display:none;' }}">
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->instagram }}" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>

                        <li style="{{ isset($informations->linkedin)!=""?'':'display:none;' }}">
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->linkedin }}" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>

                        <li style="{{ isset($informations->twitter)!=""?'':'display:none;' }}"><span
                                class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->twitter }}" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a></span>
                        </li>
                        <li style="{{ isset($informations->pinterest)!=""?'':'display:none;' }}">
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->pinterest }}" target="_blank">
                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>
                    </ul>
                </div>

            </div>
        </footer>
        <!--/Footer -->


    </div>
    <!--/Content Scroll -->

    @endsection
