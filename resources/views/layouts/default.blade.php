<!DOCTYPE html>
<html lang="pt-BR">

<head>

    @if (isset($title))
        <title>{{ $title }} - Sila Decor</title>
    @else
        <title>Sila Decor - Móveis</title>
    @endif

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Sila Decor - Móveis planejados" />
    <meta name="author" content="Sila Decor">
    <meta charset="UTF-8" />
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />

    {{-- Os seguintes estilos estão sendo importados em sytle.css
        -shortcode.css
        -portfolio.css
        -showcase.css
        -assets.css
    Mudar a referência ao subir para a hospedagem --}}

    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-153431948-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-153431948-1');

    </script>

    @yield('social-tags')

</head>

<body class="hidden hidden-ball smooth-scroll">

    <main>
        <!-- Preloader -->
        <div class="preloader-wrap">
            <div class="outer">
                <div class="inner">
                    <div class="percentage" id="precent"></div>
                    <div class="trackbar">
                        <div class="loadbar"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--/Preloader -->

        <div class="cd-index cd-main-content">

            <!-- Page Content -->
            <div id="page-content" class="light-content">

                <!-- Header -->
                <header class="classic-menu">
                    <div id="header-container">


                        <!-- Logo -->
                        <div id="logo" class="hide-ball">
                            <a class="ajax-link" data-type="page-transition" href="index.html">
                                <a href="{{ route('nav.index') }}"><img class="white-logo"
                                        src="{{ asset('images/logo.png') }}" alt="Sila Decor"></a>
                            </a>
                        </div>
                        <!--/Logo -->


                        {{-- <div class="header-middle">
                	<div class="button-wrap center quickmenu">
                        <div class="icon-wrap parallax-wrap">
                            <div class="button-icon parallax-element">
                                <div class="plus-img">
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="button-text"><span data-on="Nossos Produtos" data-off="Fechar">Nossos Produtos</span></div>
                    </div>
                </div> --}}


                        <!-- Navigation -->
                        <nav>
                            <div class="nav-height">
                                <div class="outer">
                                    <div class="inner">
                                        <ul data-breakpoint="1025" class="flexnav">
                                            {{-- <li class="ajax-link">
                                            <div class="header-middle">
                                                <div class="button-wrap left quickmenu">
                                                    <div class="icon-wrap parallax-wrap">
                                                        <div class="button-icon parallax-element">
                                                            <div class="plus-img">
                                                                <span></span>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="button-text"><span data-on="Nossos Produtos" data-off="Fechar">Nossos Produtos</span></div>
                                            </div>
                                        </div>
                                    </li> --}}
                                            <li class="link menu-timeline"><a class="active" href="javascript:;"><span
                                                        data-hover="Produtos">Produtos</span></a>
                                                <ul>
                                                    @foreach ($categories as $category)
                                                    @if ($category->active == 1)
                                                    <li>
                                                        <a class="ajax-link {{ Request::is($category->url)?"active":"" }}"
                                                            href="{{ route('nav.categoria', ['categoria' => $category->url]) }}"
                                                            data-type="page-transition">{{ $category->title }}
                                                        </a>
                                                    </li>
                                                    @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                            <li class="link menu-timeline"><a class="ajax-link"
                                                    data-type="page-transition" href="{{ route('nav.sobre') }}"><span
                                                        data-hover="Sila Decor">Sila Decor</span></a></li>
                                            <li class="link menu-timeline"><a class="ajax-link"
                                                    data-type="page-transition" href="{{ route('nav.contato') }}"><span
                                                        data-hover="Contato">Contato</span></a></li>
                                            <li class="link menu-timeline whats-contact"><a target="_blank"
                                                    href="https://wa.me/+55<?= preg_replace('/[^0-9]+/','', $informations->whatsapp)?>"><i
                                                        class="fa fa-whatsapp" aria-hidden="true"></i> (17)
                                                    99728-3179</a></li>
                                            <li class="link menu-timeline" style="padding-top: 0px;">
                                                <div class="search-input" style="display: inline-flex">
                                                    <span>
                                                        <form action="{{ route('nav.procurar') }}" method="GET">
                                                            <input name="search" type="text"
                                                                placeholder="Procure por..." size="12">
                                                        </form>
                                                    </span>
                                                </div>
                                                <div class="search-icon" style="display: inline-flex">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </nav>
                        <!--/Navigation -->

                        <!-- Menu Burger -->
                        <div class="button-wrap right  menu">
                            <div class="icon-wrap parallax-wrap">
                                <div class="button-icon parallax-element">
                                    <div id="burger-wrapper">
                                        <div id="menu-burger">
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-text"><span data-hover="Menu">Menu</span></div>
                        </div>
                        <!--/Menu Burger -->

                    </div>
                </header>
                <!--/Header -->


                <!-- Quick Menu -->
                {{-- <div id="quickmenu">

                    <div class="outer">
                        <div class="inner">
                            <div id="quickmenu-scroll">
                                <div id="close-quickmenu"></div>
                                <ul id="quick-projects" data-fx="1">
                                    <li class="hide-ball" data-img="images/tela2.jpg">
                                        <a class="quick-title" data-type="page-transition" href="sofas.html">
                                            <div class="q-timeline">SOFÁS<div class="q-cat">Sofás personalizados</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="hide-ball" data-img="images/tela1.jpg">
                                        <a class="quick-title" data-type="page-transition" href="poltronas.html">
                                            <div class="q-timeline">POLTRONAS<div class="q-cat">Poltronas exclusivas
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="hide-ball" data-img="images/tela4.jpg">
                                        <a class="quick-title" data-type="page-transition" href="aparadores.html">
                                            <div class="q-timeline">APARADORES<div class="q-cat">Aparadores exlusivos
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="hide-ball" data-img="images/tela3.jpg">
                                        <a class="quick-title" data-type="page-transition" href="buffet.html">
                                            <div class="q-timeline">BUFFET<div class="q-cat">Buffet exlusivos</div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> --}}

                @yield('content')



                <div class="thumb-container">
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                    <div class="thumb-page"></div>
                </div>




            </div>
            <!--/Page Content -->

        </div>
    </main>


    <div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
            <div id="hold-event"></div>
            <div id="ball-loader"></div>
        </div>
    </div>
    <div id="clone-image"></div>
    <div id="rotate-device"></div>

    <div id="ohsnap"></div>

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpK1sWi3J3EbUOkF_K4-UHzi285HyFX5M&sensor=false"></script>
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/ohsnap.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('vendors/jquery.validate/jquery.validate.min.js') }}"></script>

</body>

</html>
