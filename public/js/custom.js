$(document).ready(function () {
    (function ($) {
        "use strict";
        jQuery.validator.addMethod('answercheck', function (value, element) {
            return this.optional(element) || /^\bcat\b$/.test(value)
        }, "type the correct answer -_-");
        $(function () {
            $('#contactForm').validate({
                rules: {
                    name: {
                        required: !0,
                        minlength: 2
                    },
                    email: {
                        required: !0,
                        email: !0
                    },
                    comments: {
                        required: !0,
                        minlength: 20
                    }
                },
                messages: {
                    name: {
                        required: "Por favor informe seu nome",
                        minlength: "Seu nome precisa conter ao menos 2 caracteres"
                    },
                    email: {
                        required: "Por favor informe seu e-mail"
                    },
                    comments: {
                        required: "Sua mensagem não pode ser vazia",
                        minlength: "Sua mensagem não pode ter menos de 20 caracteres"
                    }
                },
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        type: "POST",
                        url: $(form).attr('action'),
                        beforeSend: function (data) {
                            $("#contactForm :input").prop("disabled", !0)
                        },
                        success: function (data) {
                            ohSnap('Mensagem enviada com sucesso.', {
                                'duration': '2000'
                            })
                        },
                        error: function (data) {
                            ohSnap('Falha ao enviar mensagem', {
                                color: 'red'
                            })
                        }
                    })
                }
            })
        })
    })(jQuery)
})

$(document).ready(function () {

    $('.section-image').each(function () {
        if ($(window).width() < 440) {
            $(this).css('background-image', 'url(' + $(this).data('mobile') + ')');
            $('#hero-bg-image').addClass('mobile');
        } else {
            $(this).css('background-image', 'url(' + $(this).data('src') + ')');
        }
    });

    $(window).resize(function () {

        $('.section-image').each(function () {
            if ($(window).width() < 440) {
                $(this).css('background-image', 'url(' + $(this).data('mobile') + ')');
                $('#hero-bg-image').addClass('mobile');
            } else {
                $(this).css('background-image', 'url(' + $(this).data('src') + ')');
            }
        });

    });

});
