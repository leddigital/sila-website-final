-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: sila
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text,
  `info2` text,
  `info3` text,
  `link1` text,
  `link2` text,
  `link3` text,
  `path` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `banners` text,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  `bannerMobile` varchar(255) NOT NULL DEFAULT 'NULL',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (11,'Mesas','mesas','Em diversos formatos e com variação de lugares, nossas mesas destacam qualquer ambiente. Os modelos encantam e a sua casa agradece.',NULL,'categoria_5dc9b79164687.jpeg',1,1,'categoria_mobile_5dee5bee54625.jpeg'),(12,'Buffets','buffets','Modernos e luxuosos, eles garantem um novo visual para o seu ambiente. São de fácil adaptação e combinam perfeitamente com vários tipos de espaços.',NULL,'categoria_5dc9b6c5c85d8.jpeg',1,1,'categoria_mobile_5dee8ac7c6ad8.jpeg'),(13,'Cadeiras','cadeiras','Conforto é a palavra-chave. Para ter a melhor experiência, trabalhamos com materiais de primeira para aumentar o seu bem-estar.',NULL,'categoria_5dc9b6d554e6d.jpeg',1,1,'categoria_mobile_5dee5bdbf318a.jpeg'),(14,'Poltronas','poltronas','Nossas poltronas agregam sofisticação ao ambiente e oferecem máximo conforto em quem as experimenta. Uma peça indispensável para salas e quartos.',NULL,'categoria_5dcd5c82dd586.jpeg',1,1,'categoria_mobile_5dee5c164f3c3.jpeg'),(15,'Aparadores','aparadores','Estes móveis garantem uma nova aparência para ambientes sofisticados e que prezam por cada detalhe.',NULL,'categoria_5dc9b78396432.jpeg',1,1,'categoria_mobile_5dee5bc9bd88a.jpeg'),(16,'Mesas de centro','mesas-de-centro','Úteis para decorar, preencher e valorizar salas e espaços comuns. A qualidade das nossas mesas de centro surpreende e encanta.',NULL,'categoria_5dc9b7753e593.jpeg',1,1,'categoria_mobile_5dee5bff87164.jpeg'),(17,'Mesas laterais','mesas-laterais','Práticas, as mesas laterais são produtos versáteis para diversos tipos de ambiente',NULL,'categoria_5dc5c4f37a29f.jpeg',0,0,'NULL'),(19,'Decoração','decoracao','O ponto máximo para encantar seus convidados, uma boa decoração garante um ambiente dinâmico e exuberante. Conheça a nossa linha.',NULL,'categoria_5dc5c59856269.jpeg',0,0,'NULL'),(20,'Quadros','quadros','A arte deve ser exposta em ambientes que as valorizem. A combinação dos quadros com nossos móveis repagina totalmente qualquer ambiente.',NULL,'categoria_5dc5c64006357.jpeg',0,0,'NULL'),(21,'Tapetes','tapetes','As peças encantam em seus diversos formatos e tamanhos. Tapetem dão um acabamento fantástico para ambientes e merecem atenção na hora da escolha. Temos vastas opções.',NULL,'categoria_5dc5c6afbd93f.jpeg',0,0,'NULL'),(22,'Luminárias','luminarias','Se a iluminação pode transformar ambientes, luminárias com desenhos especiais podem agregar ainda mais à decoração do seu espaço.',NULL,'categoria_5dc5c793745d2.jpeg',0,0,'NULL');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_has_contents`
--

DROP TABLE IF EXISTS `categories_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL,
  PRIMARY KEY (`categories_id`,`contents_id`),
  KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  KEY `fk_categories_has_contents_categories1_idx` (`categories_id`),
  CONSTRAINT `fk_categories_has_contents_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categories_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_has_contents`
--

LOCK TABLES `categories_has_contents` WRITE;
/*!40000 ALTER TABLE `categories_has_contents` DISABLE KEYS */;
INSERT INTO `categories_has_contents` VALUES (15,49),(14,50),(15,51),(15,52),(15,53),(15,54),(15,55),(12,56),(12,57),(12,58),(12,59),(12,60),(12,61),(12,62),(12,64),(12,65),(12,66),(12,67),(13,69),(13,70),(13,71),(13,72),(13,73),(13,74),(13,75),(13,76),(13,77),(13,78),(13,79),(13,80),(13,81),(13,82),(13,83),(13,84),(13,85),(13,86),(13,87),(13,88),(13,89),(13,90),(13,91),(13,92),(13,93),(13,94),(13,95),(11,96),(11,97),(11,98),(11,99),(11,100),(16,101),(16,103),(11,104),(11,105),(11,106),(11,107),(11,108),(11,109),(11,110),(11,111),(11,112),(11,113),(11,114),(11,115),(11,116),(11,117),(11,118),(11,119),(11,120),(11,121),(11,122),(11,123),(11,124),(11,125),(11,126),(11,127),(11,128),(11,129),(14,131);
/*!40000 ALTER TABLE `categories_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `short_description` text,
  `content` longtext,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alta` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (49,'Aparador Camila',NULL,NULL,NULL,'produto','aparador-camila','produto_5dcace135d327.jpeg','alta_5dcace139b1cd.jpeg','2019-11-11 19:57:12','2019-11-12 15:21:55'),(50,'Poltrona Deise',NULL,NULL,NULL,'produto','poltrona-deise','produto_5dc9be6d2b330.jpeg','alta_5dc9be6d2b500.jpeg','2019-11-11 20:01:37','2019-11-11 20:02:53'),(51,'Aparador/Bar Dora',NULL,NULL,NULL,'produto','aparador-bar-dora','produto_5dcacdebd37ca.jpeg','alta_5dcacdec00789.jpeg','2019-11-12 15:21:16','2019-11-12 15:21:16'),(52,'Aparador Maisa',NULL,NULL,NULL,'produto','aparador-maisa','produto_5dcace49f2492.jpeg','alta_5dcace49f25ca.jpeg','2019-11-12 15:22:49','2019-11-12 15:22:49'),(53,'Aparador Murano',NULL,NULL,NULL,'produto','aparador-murano','produto_5dcace6890bb1.jpeg','alta_5dcace6890d13.jpeg','2019-11-12 15:23:20','2019-11-12 15:23:20'),(54,'Aparador Thalita',NULL,NULL,NULL,'produto','aparador-thalita','produto_5dcace861becb.jpeg','alta_5dcace861c02a.jpeg','2019-11-12 15:23:50','2019-11-12 15:23:50'),(55,'Aparador Xapuri',NULL,NULL,NULL,'produto','aparador-xapuri','produto_5dcacea6da3aa.jpeg','alta_5dcacea6da548.jpeg','2019-11-12 15:24:22','2019-11-12 15:24:22'),(56,'Buffet Carla',NULL,NULL,NULL,'produto','buffet-carla','produto_5dcacec1145fb.jpeg','alta_5dcacec1212ac.jpeg','2019-11-12 15:24:49','2019-11-12 15:24:49'),(57,'Buffet Athenas',NULL,NULL,NULL,'produto','buffet-athenas','produto_5dcacedd41109.jpeg','alta_5dcacedd52fe0.jpeg','2019-11-12 15:25:17','2019-11-12 15:25:17'),(58,'Buffet Galli',NULL,NULL,NULL,'produto','buffet-galli','produto_5dcacf64b9b93.jpeg','alta_5dcacf64b9d5e.jpeg','2019-11-12 15:27:32','2019-11-12 15:27:32'),(59,'Buffet Galli',NULL,NULL,NULL,'produto','buffet-galli-1','produto_5dcacf7d5c3fc.jpeg','alta_5dcacf7d5c5ae.jpeg','2019-11-12 15:27:57','2019-11-12 15:27:57'),(60,'Buffet New Vicenza',NULL,NULL,NULL,'produto','buffet-new-vicenza','produto_5dcacfd2c96cc.jpeg','alta_5dcacfd2c982e.jpeg','2019-11-12 15:29:22','2019-11-12 15:29:22'),(61,'Buffet Prisma',NULL,NULL,NULL,'produto','buffet-prisma','produto_5dcad00b2d972.jpeg','alta_5dcad00b2db57.jpeg','2019-11-12 15:30:19','2019-11-12 15:30:19'),(62,'Buffet Regis com cristaleiras',NULL,NULL,NULL,'produto','buffet-regis-com-cristaleiras','produto_5dcad07dc29e7.jpeg','alta_5dcad07dc2ca3.jpeg','2019-11-12 15:32:13','2019-11-12 15:32:13'),(64,'Buffet Riviera',NULL,NULL,NULL,'produto','buffet-riviera','produto_5dcad0a29837b.jpeg','alta_5dcad0a2984ea.jpeg','2019-11-12 15:32:50','2019-11-12 15:32:50'),(65,'Buffet Samantha',NULL,NULL,NULL,'produto','buffet-samantha','produto_5dcad0c1a6170.jpeg','alta_5dcad0c1a642e.jpeg','2019-11-12 15:33:21','2019-11-12 15:33:21'),(66,'Buffet Trend',NULL,NULL,NULL,'produto','buffet-trend','produto_5dcad0dfca547.jpeg','alta_5dcad0e029f5d.jpeg','2019-11-12 15:33:52','2019-11-12 15:33:52'),(67,'Buffet Xapuri',NULL,NULL,NULL,'produto','buffet-xapuri','produto_5dcad0fd7ca4c.jpeg','alta_5dcad0fd7ccac.jpeg','2019-11-12 15:34:21','2019-11-12 15:34:21'),(69,'Cadeira Asti',NULL,NULL,NULL,'produto','cadeira-asti','produto_5dcad1218d918.jpeg','alta_5dcad1218dac3.jpeg','2019-11-12 15:34:57','2019-11-12 15:34:57'),(70,'Cadeira Athenas',NULL,NULL,NULL,'produto','cadeira-athenas','produto_5dcad18c83ae2.jpeg','alta_5dcad18c83c7d.jpeg','2019-11-12 15:36:44','2019-11-12 15:36:44'),(71,'Cadeira Athenas',NULL,NULL,NULL,'produto','cadeira-athenas-1','produto_5dcad1cd60ef6.jpeg','alta_5dcad1cd6dd6f.jpeg','2019-11-12 15:37:49','2019-11-12 15:37:49'),(72,'Cadeira Bianca',NULL,NULL,NULL,'produto','cadeira-bianca','produto_5dcad1e88b517.jpeg','alta_5dcad1e88b6ae.jpeg','2019-11-12 15:38:16','2019-11-12 15:38:16'),(73,'Cadeira Bruna',NULL,NULL,NULL,'produto','cadeira-bruna','produto_5dcad2275daa1.jpeg','alta_5dcad2275dc14.jpeg','2019-11-12 15:39:19','2019-11-12 15:39:19'),(74,'Cadeira Camila',NULL,NULL,NULL,'produto','cadeira-camila','produto_5dcad246b8a25.jpeg','alta_5dcad246b8c1f.jpeg','2019-11-12 15:39:50','2019-11-12 15:39:50'),(75,'Cadeira Camila com estofado',NULL,NULL,NULL,'produto','cadeira-camila-com-estofado','produto_5dcad271990e3.jpeg','alta_5dcad271a19ca.jpeg','2019-11-12 15:40:33','2019-11-12 15:40:33'),(76,'Cadeira Carla',NULL,NULL,NULL,'produto','cadeira-carla','produto_5dcad298ce3b9.jpeg','alta_5dcad298ce559.jpeg','2019-11-12 15:41:12','2019-11-12 15:41:12'),(77,'Cadeira Diamante',NULL,NULL,NULL,'produto','cadeira-diamante','produto_5dcad2c285260.jpeg','alta_5dcad2c28543e.jpeg','2019-11-12 15:41:54','2019-11-12 15:41:54'),(78,'Cadeira Dora',NULL,NULL,NULL,'produto','cadeira-dora','produto_5dcad2dbc093d.jpeg','alta_5dcad2dbc0b65.jpeg','2019-11-12 15:42:19','2019-11-12 15:42:19'),(79,'Cadeira Elbrus',NULL,NULL,NULL,'produto','cadeira-elbrus','produto_5dcad32a5f9b9.jpeg','alta_5dcad32a5fb46.jpeg','2019-11-12 15:43:38','2019-11-12 15:43:38'),(80,'Cadeira Elbrus com braço',NULL,NULL,NULL,'produto','cadeira-elbrus-com-braco','produto_5dcad348c83a4.jpeg','alta_5dcad348c85c7.jpeg','2019-11-12 15:44:08','2019-11-26 19:19:19'),(81,'Cadeira Fernanda',NULL,NULL,NULL,'produto','cadeira-fernanda','produto_5dcad36cb5f7f.jpeg','alta_5dcad36cb616f.jpeg','2019-11-12 15:44:44','2019-11-12 15:44:44'),(82,'Cadeira Fernanda com braço',NULL,NULL,NULL,'produto','cadeira-fernanda-com-braco','produto_5dcad395bbc41.jpeg','alta_5dcad3960ce9f.jpeg','2019-11-12 15:45:26','2019-11-12 15:45:26'),(83,'Cadeira Galan',NULL,NULL,NULL,'produto','cadeira-galan','produto_5dcad3e553268.jpeg','alta_5dcad3e5533d5.jpeg','2019-11-12 15:46:45','2019-11-12 15:46:45'),(84,'Cadeira Imperial',NULL,NULL,NULL,'produto','cadeira-imperial','produto_5dcad4022d363.jpeg','alta_5dcad4022d546.jpeg','2019-11-12 15:47:14','2019-11-12 15:47:14'),(85,'Cadeira Las Vegas',NULL,NULL,NULL,'produto','cadeira-las-vegas','produto_5dcad4234133c.jpeg','alta_5dcad423415a7.jpeg','2019-11-12 15:47:47','2019-11-12 15:47:47'),(86,'Cadeira Maisa',NULL,NULL,NULL,'produto','cadeira-maisa','produto_5dcad44048cdf.jpeg','alta_5dcad44048e8e.jpeg','2019-11-12 15:48:16','2019-11-12 15:48:16'),(87,'Cadeira Mirella',NULL,NULL,NULL,'produto','cadeira-mirella','produto_5dcad49f1d791.jpeg','alta_5dcad49f1d965.jpeg','2019-11-12 15:49:51','2019-11-12 15:49:51'),(88,'Cadeira Natalia',NULL,NULL,NULL,'produto','cadeira-natalia','produto_5dcad4ca3642a.jpeg','alta_5dcad4ca365de.jpeg','2019-11-12 15:50:34','2019-11-12 15:50:34'),(89,'Cadeira New Vicenza',NULL,NULL,NULL,'produto','cadeira-new-vicenza','produto_5dcad51164a7d.jpeg','alta_5dcad51167f91.jpeg','2019-11-12 15:51:45','2019-11-12 15:51:45'),(90,'Cadeira Parvati',NULL,NULL,NULL,'produto','cadeira-parvati','produto_5dcad54136a5d.jpeg','alta_5dcad541746c6.jpeg','2019-11-12 15:52:33','2019-11-12 15:52:33'),(91,'Cadeira Ravena',NULL,NULL,NULL,'produto','cadeira-ravena','produto_5dcad55c86fe0.jpeg','alta_5dcad55c87181.jpeg','2019-11-12 15:53:00','2019-11-12 15:53:00'),(92,'Cadeira Requinte',NULL,NULL,NULL,'produto','cadeira-requinte','produto_5dcad58cc2edf.jpeg','alta_5dcad58cc3009.jpeg','2019-11-12 15:53:48','2019-11-12 15:53:48'),(93,'Cadeira Riviera',NULL,NULL,NULL,'produto','cadeira-riviera','produto_5dcad5ad63b86.jpeg','alta_5dcad5ad63d1b.jpeg','2019-11-12 15:54:21','2019-11-12 15:54:21'),(94,'Cadeira Simone',NULL,NULL,NULL,'produto','cadeira-simone','produto_5dcad5cb35081.jpeg','alta_5dcad5cb35340.jpeg','2019-11-12 15:54:51','2019-11-12 15:54:51'),(95,'Cadeira Tais',NULL,NULL,NULL,'produto','cadeira-tais','produto_5dcad5ea98453.jpeg','alta_5dcad5ea985f0.jpeg','2019-11-12 15:55:22','2019-11-12 15:55:22'),(96,'Mesa Athenas',NULL,NULL,NULL,'produto','mesa-athenas','produto_5dcad608c7f4a.jpeg','alta_5dcad608c8086.jpeg','2019-11-12 15:55:52','2019-11-12 15:55:52'),(97,'Mesa Athenas',NULL,NULL,NULL,'produto','mesa-athenas-1','produto_5dcad62110e0e.jpeg','alta_5dcad62110f77.jpeg','2019-11-12 15:56:17','2019-11-12 15:56:17'),(98,'Mesa Bianca',NULL,NULL,NULL,'produto','mesa-bianca','produto_5dcad6836182e.jpeg','alta_5dcad683d4da4.jpeg','2019-11-12 15:57:55','2019-11-12 15:57:55'),(99,'Mesa Camila',NULL,NULL,NULL,'produto','mesa-camila','produto_5dcad6a22c8e1.jpeg','alta_5dcad6a22ca11.jpeg','2019-11-12 15:58:26','2019-11-12 15:58:26'),(100,'Mesa Carla',NULL,NULL,NULL,'produto','mesa-carla','produto_5dcad6c1e3c0b.jpeg','alta_5dcad6c1e3d8b.jpeg','2019-11-12 15:58:57','2019-11-12 15:58:57'),(101,'Mesa de centro Brasilia',NULL,NULL,NULL,'produto','mesa-de-centro-brasilia','produto_5dcad6f040754.jpeg','alta_5dcad6f0408ae.jpeg','2019-11-12 15:59:44','2019-11-12 15:59:44'),(102,'Mesa de centro Murano',NULL,NULL,NULL,'produto','mesa-de-centro-murano','produto_5dcad710cad22.jpeg','alta_5dcad710eff5c.jpeg','2019-11-12 16:00:17','2019-11-12 16:00:17'),(103,'Mesa de centro Sila',NULL,NULL,NULL,'produto','mesa-de-centro-sila','produto_5dcad72ab993e.jpeg','alta_5dcad72ab9a94.jpeg','2019-11-12 16:00:42','2019-11-12 16:00:42'),(104,'Mesa Clara',NULL,NULL,NULL,'produto','mesa-clara','produto_5dcad75097c6b.jpeg','alta_5dcad75097e34.jpeg','2019-11-12 16:01:20','2019-11-12 16:01:20'),(105,'Mesa Emanueli',NULL,NULL,NULL,'produto','mesa-emanueli','produto_5dcad782160e6.jpeg','alta_5dcad78216232.jpeg','2019-11-12 16:02:10','2019-11-12 16:02:10'),(106,'Mesa Fernanda',NULL,NULL,NULL,'produto','mesa-fernanda','produto_5dcad79cb74e9.jpeg','alta_5dcad79cba3d9.jpeg','2019-11-12 16:02:36','2019-11-12 16:02:36'),(107,'Mesa Future',NULL,NULL,NULL,'produto','mesa-future','produto_5dcad7bfd7a07.jpeg','alta_5dcad7bfd7b30.jpeg','2019-11-12 16:03:11','2019-11-12 16:03:11'),(108,'Mesa Gabriela',NULL,NULL,NULL,'produto','mesa-gabriela','produto_5dcad7ddef7cb.jpeg','alta_5dcad7ddef9ba.jpeg','2019-11-12 16:03:41','2019-11-12 16:03:41'),(109,'Mesa Gabriela',NULL,NULL,NULL,'produto','mesa-gabriela-1','produto_5dcad7de1ba71.jpeg','alta_5dcad7de1bbd2.jpeg','2019-11-12 16:03:42','2019-11-12 16:03:42'),(110,'Mesa Garden',NULL,NULL,NULL,'produto','mesa-garden','produto_5dcad7fa03160.jpeg','alta_5dcad7fa304eb.jpeg','2019-11-12 16:04:10','2019-11-12 16:04:10'),(111,'Mesa Karina',NULL,NULL,NULL,'produto','mesa-karina','produto_5dcad81468dd0.jpeg','alta_5dcad81468ef6.jpeg','2019-11-12 16:04:36','2019-11-12 16:04:36'),(112,'Mesa Kelly',NULL,NULL,NULL,'produto','mesa-kelly','produto_5dcad82f7c278.jpeg','alta_5dcad82f7c4b2.jpeg','2019-11-12 16:05:03','2019-11-12 16:05:03'),(113,'Mesa Las Vegas',NULL,NULL,NULL,'produto','mesa-las-vegas','produto_5dcad84aa7d7f.jpeg','alta_5dcad84aa7f55.jpeg','2019-11-12 16:05:30','2019-11-12 16:05:30'),(114,'Mesa Lorenza',NULL,NULL,NULL,'produto','mesa-lorenza','produto_5dcad863f1092.jpeg','alta_5dcad863f11b4.jpeg','2019-11-12 16:05:55','2019-11-12 16:05:55'),(115,'Mesa Lyra',NULL,NULL,NULL,'produto','mesa-lyra','produto_5dcad87c0efef.jpeg','alta_5dcad87c0f14f.jpeg','2019-11-12 16:06:20','2019-11-12 16:06:20'),(116,'Mesa Maisa',NULL,NULL,NULL,'produto','mesa-maisa','produto_5dcad894517c2.jpeg','alta_5dcad894518e6.jpeg','2019-11-12 16:06:44','2019-11-12 16:06:44'),(117,'Mesa Maisa',NULL,NULL,NULL,'produto','mesa-maisa-1','produto_5dcad89507707.jpeg','alta_5dcad895078da.jpeg','2019-11-12 16:06:45','2019-11-12 16:06:45'),(118,'Mesa Murano',NULL,NULL,NULL,'produto','mesa-murano','produto_5dcad8af7a45b.jpeg','alta_5dcad8afb7cfe.jpeg','2019-11-12 16:07:11','2019-11-12 16:07:11'),(119,'Mesa New Coliseu',NULL,NULL,NULL,'produto','mesa-new-coliseu','produto_5dcad8c998cee.jpeg','alta_5dcad8c998e31.jpeg','2019-11-12 16:07:37','2019-11-12 16:07:37'),(120,'Mesa New Vicenza',NULL,NULL,NULL,'produto','mesa-new-vicenza','produto_5dcad8ff795f1.jpeg','alta_5dcad8ffa2148.jpeg','2019-11-12 16:08:31','2019-11-12 16:08:31'),(121,'Mesa Pamela',NULL,NULL,NULL,'produto','mesa-pamela','produto_5dcad917349aa.jpeg','alta_5dcad91734ac7.jpeg','2019-11-12 16:08:55','2019-11-12 16:08:55'),(122,'Mesa Parvati',NULL,NULL,NULL,'produto','mesa-parvati','produto_5dcad93853bf3.jpeg','alta_5dcad93853d44.jpeg','2019-11-12 16:09:28','2019-11-12 16:09:28'),(123,'Mesa Requinte',NULL,NULL,NULL,'produto','mesa-requinte','produto_5dcad9550c5c9.jpeg','alta_5dcad9550c723.jpeg','2019-11-12 16:09:57','2019-11-12 16:09:57'),(124,'Mesa Requinte Dupla',NULL,NULL,NULL,'produto','mesa-requinte-dupla','produto_5dcad96df0a5e.jpeg','alta_5dcad96e01b01.jpeg','2019-11-12 16:10:22','2019-11-12 16:10:22'),(125,'Mesa Riviera',NULL,NULL,NULL,'produto','mesa-riviera','produto_5dcad987d4e2a.jpeg','alta_5dcad987d4fe0.jpeg','2019-11-12 16:10:47','2019-11-12 16:10:47'),(126,'Mesa Samantha',NULL,NULL,NULL,'produto','mesa-samantha','produto_5dcad99d215b8.jpeg','alta_5dcad99d216e4.jpeg','2019-11-12 16:11:09','2019-11-12 16:11:09'),(127,'Mesa Sandra',NULL,NULL,NULL,'produto','mesa-sandra','produto_5dcad9b74dabd.jpeg','alta_5dcad9b74dbf7.jpeg','2019-11-12 16:11:35','2019-11-12 16:11:35'),(128,'Mesa Sila',NULL,NULL,NULL,'produto','mesa-sila','produto_5dcad9c8b1124.jpeg','alta_5dcad9c8b1271.jpeg','2019-11-12 16:11:52','2019-11-12 16:11:52'),(129,'Mesa Thalita',NULL,NULL,NULL,'produto','mesa-thalita','produto_5dcad9e1182a3.jpeg','alta_5dcad9e1183a3.jpeg','2019-11-12 16:12:17','2019-11-12 16:12:17'),(131,'Poltrona Folha',NULL,NULL,NULL,'produto','poltrona-folha','produto_5dcae56cec7a8.jpeg','alta_5dcae56cec9e1.jpeg','2019-11-12 17:01:32','2019-11-12 17:01:32');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_contents`
--

DROP TABLE IF EXISTS `contents_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL,
  PRIMARY KEY (`contents_id`,`contents_child_id`),
  KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  KEY `fk_contents_has_contents_contents1_idx` (`contents_id`),
  CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_contents`
--

LOCK TABLES `contents_has_contents` WRITE;
/*!40000 ALTER TABLE `contents_has_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_images`
--

DROP TABLE IF EXISTS `contents_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text,
  PRIMARY KEY (`id`,`contents_id`),
  KEY `fk_contents_images_contents_idx` (`contents_id`),
  CONSTRAINT `fk_contents_images_contents` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_images`
--

LOCK TABLES `contents_images` WRITE;
/*!40000 ALTER TABLE `contents_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text,
  `facebook` text,
  `linkedin` text,
  `twitter` text,
  `pinterest` text,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text,
  `phone2` varchar(45) DEFAULT NULL,
  `texto_1` text,
  `texto_2` text,
  `valor_1` text,
  `valor_2` text,
  `valor_3` text,
  `valor_4` text,
  `valor_5` text,
  `valor_texto_1` text,
  `valor_texto_2` text,
  `valor_texto_3` text,
  `valor_texto_4` text,
  `valor_texto_5` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'Av. Marginal Tancredo Neves','300','Parque Industrial Tancredo Neves','15076-630','São José do Rio Preto','SP','(17) 99728-3179','https://www.instagram.com/siladecorr/','https://www.facebook.com/siladecorr/',NULL,NULL,NULL,'(17) 99728-3179','contato@siladecor.com.br','(17) 99728-3179','Somos especializados em salas, sofás e decoração. Nossa seleção de produtos apresenta traços contemporâneos, atemporais, e com personalidade única. Perfeito para a sua casa.','Para manter a qualidade dos nossos produtos, trabalhamos apenas com parceiros e fornecedores com padrão de qualidade comprovados e certificados. Fazemos de tudo para levar segurança e a certeza de um bom investimento para você. ','FABRICAÇÃO PRÓPRIA','DESIGN E EXCLUSIVIDADE','ENTREGA EFICIENTE','GARANTIA','SUSTENTABILIDADE','Um dos diferenciais é a qualidade que só uma loja de fábrica pode oferecer. \r\nNossos produtos são feitos com cuidado e capricho na produção. Desta forma, cada peça é projetada e executada com muito carinho e atenção aos detalhes. Por pensar nos mínimos detalhes, trabalhamos com tecnologia e pessoas capacitadas para garantir a qualidade e durabilidade da peça no seu ambiente.','Em nossa loja, você encontra peças com design exclusivo, desenhados para o seu estilo de vida: moderno e contemporâneo. Nossas peças já conquistaram prêmios pelo seu design diferenciado. ','Você chega, escolhe e nós te entregamos um produto adequado ao seu ambiente. Trabalhamos com uma ampla variedade de produtos sob encomenda. Sempre prezando por um bom prazo de confecção e entrega, para que você tenha o seu ambiente, do seu jeito, sem demora. ','Qualidade nos define. Produzimos com o mais alto padrão para que o seu novo móvel seja duradouro e você possa extrair sempre o máximo do conforto e beleza que ele oferece. Mas sempre que for necessário, estaremos prontos para te dar suporte em questões de manutenção. ','Ser elegante sem ser sustentável não faz sentido para nós. Por isso, trabalhamos com matérias-primas de qualidade e procedência. Nossos fornecedores são certificados e, da mesma forma que nós, comprometidos com o meio ambiente. ');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (5,'Camila','financeiro@moveismaschieto.com.br',NULL,'$2y$10$6bWLGVtm8.XFlpytEACw3OOOx7IxR9QJM.d4V1UGW0puYI/DbY3IO','SwT30ZJ1jXilAyrOyHxknYEf97UXCDEOxxmbIdTcfpkJujEBXpWPewgNsunG','2019-11-26 20:54:17','2019-11-26 20:54:17'),(6,'Agencia LED','digital@agencialed.com.br',NULL,'$2y$10$fW5f5sCSvpgvwm3nKxqLleyb7WPd.UWgAu/H/X5CfHY6DrAEXM8nm','RxSbB7uMgsolHZc9mGXY8zBHMf47vtP0gEBVhnUMgWYZYKY1QgSCDxTEn2bV','2019-11-29 17:52:34','2019-11-29 17:52:34');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'sila'
--

--
-- Dumping routines for database 'sila'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-09 17:07:05
