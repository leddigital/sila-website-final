<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use App\Informations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NavigationController extends Controller
{

    public function index(Request $request) {
           $categories = Categories::orderBy('title','asc')->where('featured','=','1')->get()->all();

        return view('pages.index',['banners' =>$categories]);
    }

    public function categoria(Request $request) {

        $url = $request->route('categoria');
        $categoria = Categories::with('contents')->where('url', '=', $url)->first();
        $nextCat = Categories::where('id', '>', $categoria->id)->where('featured', '=', '1')
        ->get()->first();

        if(!isset($nextCat)) {
            $nextCat = Categories::get()->where('featured', '=', '1')->first();
        }

        $title = $categoria->title;
        return view('pages.categoria',
            ['categoria' => $categoria, 'nextCat' => $nextCat, 'title' => $title]);
    }

    public function sobre(Request $request) {
        $title = "Sobre";
        return view('pages.sobre', ['title' => $title]);
    }

    public function contato(Request $request) {
        $title = "Contato";
        return view('pages.contato', ['title' => $title]);
    }

    public function procurar(Request $request) {

        $form = $request->all();
        $itens = Contents::where('title', 'like', '%' . $form['search'] . '%')->get()->all();
        $nextCat = Categories::all()->random();
        $title = "Procurando por '" . $form['search'] . "'";

        return view('pages.procurar',
            ['form' => $form['search'], 'itens' => $itens, 'nextCat' => $nextCat, 'title' => $title]);

    }

}
