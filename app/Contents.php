<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contents extends Model
{
    public $timestamps = true;

    protected $primaryKey = "id";
    protected $table = "contents";

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'content',
        'image',
        'image_alta',
        'type',
        'url'
    ];

    public $rules = [
        'title' => 'required',
        'type'=>'required',
    ];

    public function images(){
        return $this->hasMany('App\ContentsImages');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories','categories_has_contents', 'contents_id', 'categories_id');
    }
}
