<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public $timestamps = false;

    protected $primaryKey = "id";
    protected $table = "categories";

    protected $fillable = [
        'title',
        'short_description',
        'description',
        'banners',
        'url',
        'type',
        'featured',
        'bannerMobile'
    ];

    public function contents(){
        return $this->belongsToMany('App\Contents','categories_has_contents', 'categories_id', 'contents_id');
    }
}
