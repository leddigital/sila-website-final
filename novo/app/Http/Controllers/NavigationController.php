<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use App\Informations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NavigationController extends Controller
{

    public function index(Request $request) {
           $categories = Categories::orderBy('title','asc')->where('active','=','1')->get()->all();

        return view('pages.index',['banners' =>$categories]);
    }

    public function categoria(Request $request) {

        $url = $request->route('categoria');
        $categoria = Categories::with('contents')->where('url', '=', $url)->first();
        $nextCat = Categories::where('id', '>', $categoria->id)->get()->first();

        if(!isset($nextCat)) {
            $nextCat = Categories::get()->first();
        }

        return view('pages.categoria', ['categoria' => $categoria, 'nextCat' => $nextCat]);
    }

    public function sobre(Request $request) {
        return view('pages.sobre');
    }

    public function contato(Request $request) {
       return view('pages.contato');
    }

    public function procurar(Request $request) {

        $form = $request->all();
        $itens = Contents::where('title', 'like', '%' . $form['search'] . '%')->get()->all();
        $nextCat = Categories::all()->random();

        return view('pages.procurar', ['form' => $form['search'], 'itens' => $itens, 'nextCat' => $nextCat]);

    }

}
