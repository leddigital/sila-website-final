
@extends('layouts.default')
@section('content')

 <!-- Content Scroll -->
   <div id="content-scroll">


        <!-- Main -->
        <div id="main">

            <!-- Hero Section -->
            <div id="hero" class="has-image">
                <div id="hero-styles" class="parallax-onscroll">
                    <div id="hero-caption">
                        <div class="inner">
                            <div class="hero-title">{{ $categoria->title }}</div>
                            <div class="hero-subtitle">{{ $categoria->short_description }}</div>
                        </div>
                    </div>
                    <div class="hero-bottom">
                        <div class="hb-left">
                            <div id="scrolldown" class="text-rotate link"><span>Deslize para baixo<br />Deslize para baixo<br />Deslize para baixo<br />Deslize para baixo</span></div>
                        </div>
                        <div class="hb-right">{{ now()->year }}</div>
                    </div>
                </div>
            </div>
            <div id="hero-bg-wrapper">
                <div id="hero-image-parallax">
                    <div id="hero-bg-image" style="background-image:url({{ asset('banners/'.$categoria->banners) }})"></div>
                </div>
            </div>
            <!--/Hero Section -->


            <!-- Main Content -->
            <div id="main-content">
                <div id="main-page-content">

                    <!-- Row -->
                    <div class="vc_row row_padding_top small row_padding_bottom text-align-center">

                        <hr>

                        <h2 class="has-mask">{{ $categoria->description }}</h2>

                    </div>
                    <!--/Row -->


                    <!-- Row -->
                    <div class="vc_row full has-animation" data-delay="100">

                        <!-- Collage -->
                        <div id="justified-grid">
                            @foreach ($categoria->contents as $produto)
                                <!-- Collage item with pop-up -->
                                <div class="collage-thumb">
                                    <a class="image-link" href="{{ asset('/produtos/'.$produto->image) }}">
                                        <img src="{{ asset('/produtos/alta/'.$produto->image_alta) }}" alt="img" />
                                        <div class="thumb-info">{{ $produto->title }}</div>
                                    </a>
                                </div>
                            @endforeach
                    </div>
                    <!--/Row -->

                </div>
                <!--/Main Page Content -->



                <!-- Project Navigation -->
                <div id="project-nav">
                    <div class="next-project-wrap">
                        <div class="next-project-image">
                            {{-- <div class="next-project-image-bg" style="background-image:url(images/06hero.jpg)"></div> --}}
                        </div>
                        <div class="next-project-title">
                            <div class="inner">
                                <div class="next-subtitle-info has-animation">Continue vendo</div>
                                <div class="has-animation" data-delay="150">
                                    <a class="main-title next-ajax-link-project hide-ball" data-type="page-transition"
                                href="{{ route('nav.categoria', ['categoria' => $nextCat->url]) }}">{{ $nextCat->title }}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--/Project Navigation -->


            </div>
            <!--/Main Content -->
        </div>
        <!--/Main -->


        <!-- Footer -->
        <footer class="hidden">
            <div id="footer-container">

                <div id="backtotop" class="button-wrap left-custom">
                    <div class="icon-wrap parallax-wrap">
                        <div class="button-icon parallax-element">
                            <i class="fa fa-angle-up"></i>
                        </div>
                    </div>
                    <div class="button-text"><span data-hover="Voltar ao topo">Voltar ao topo</span></div>
                </div>

                <div class="socials-wrap">
                    <div class="socials-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                    <div class="socials-text">Siga a Sila Decor</div>
                    <ul class="socials">

                        <li style="{{ isset($informations->facebook)!=""?'':'display:none;' }}" }}>
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->facebook }}" target="_blank">
                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>

                        <li style="{{ isset($informations->instagram)!=""?'':'display:none;' }}">
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->instagram }}" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>

                        <li style="{{ isset($informations->linkedin)!=""?'':'display:none;' }}">
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->linkedin }}" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>

                        <li style="{{ isset($informations->twitter)!=""?'':'display:none;' }}"><span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->twitter }}" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a></span>
                        </li>
                        <li style="{{ isset($informations->pinterest)!=""?'':'display:none;' }}">
                            <span class="parallax-wrap">
                                <a class="parallax-element" href="{{ $informations->pinterest }}" target="_blank">
                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                </a>
                            </span>
                        </li>
                    </ul>
                </div>

            </div>
        </footer>
        <!--/Footer -->


    </div>
    <!--/Content Scroll -->

@endsection

