@extends('layouts.default')
@section('content')


<!-- Content Scroll -->
<div id="content-scroll">


    <!-- Main -->
    <div id="main">

        <!-- Hero Section -->
        <div id="hero">
            <div id="hero-styles" class="parallax-onscroll">
                <div id="hero-caption">
                    <div class="inner">
                        <div class="hero-title">CONTATO</div>
                        <div class="hero-subtitle">Envie suas críticas, sugestões e elogios para a gente</div>
                    </div>
                </div>
            </div>
        </div>
        <!--/Hero Section -->


        <!-- Main Content -->
        <div id="main-content">
            <!-- Main Page Content -->
            <div id="main-page-content">

                <!-- Row -->
                <div class="vc_row row_padding_bottom">

                    <h1 class="has-mask" data-delay="10">Vamos conversar</h1>

                    <hr>
                    <hr>

                    <div class="one_third">

                        <div class="has-animation" data-delay="100">
                            <div class="clapat-icon">
                                <i class="fa fa-paper-plane fa-2x" aria-hidden="true"></i>
                            </div>

                            <h6><a href="mailto:office@hervin.com"
                                    class="link"><span>contato@siladecor.com.br</span></a></h6>
                            <p>E-mail</p>
                        </div>

                        <div class="has-animation" data-delay="200">
                            <div class="clapat-icon">
                                <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
                            </div>

                            <h6>{{ $informations->address }}, {{ $informations->number }}</h6>
                            <h6>{{ $informations->district }}</h6>
                            <h6>{{ $informations->city }}</h6>
                            <p>Endereço</p>
                        </div>

                        <div class="has-animation" data-delay="300">
                            <div class="clapat-icon">
                                <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                            </div>

                            <h6><a href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone1) ?>"
                                    class="link"><span>{{ $informations->phone1 }}</span></a></h6>
                            <h6><a href="https://wa.me/<?= preg_replace('/[^0-9]+/','', $informations->whatsapp) ?>"
                                    class="link"><span>{{ $informations->whatsapp }}</span></a> - WhatsApp</h6>
                            <p>Telefone</p>
                        </div>

                    </div>


                    <div class="two_third last">

                        <!-- Contact Formular -->
                        <div id="contact-formular">

                            <div id="message"></div>

                            <form method="post" action="{{ route('send.mail') }}" name="contactForm"
                                id="contactForm">

                                @csrf

                                <div class="name-box has-animation" data-delay="100">
                                    <input name="name" type="text" id="name" size="30" value=""
                                        placeholder="Seu nome"><label class="input_label"></label>
                                </div>
                                <div class="email-box has-animation" data-delay="150">
                                    <input name="email" type="text" id="email" size="30" value=""
                                        placeholder="Seu e-mail"><label class="input_label"></label>
                                </div>
                                <div class="message-box has-animation" data-delay="100">
                                    <textarea name="comments" cols="40" rows="4" id="comments"
                                        placeholder="Sua mensagem"></textarea><label class="input_label slow"></label>
                                </div>
                                <div class="button-box has-animation" data-delay="100">
                                    <div class="clapat-button-wrap parallax-wrap hide-ball">
                                        <div class="clapat-button parallax-element">
                                            <div class="button-border outline rounded parallax-element-second"><input
                                                    type="submit" class="send_message" id="submit" value="Enviar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <!--/Contact Formular -->

                    </div>

                </div>
                <!--/Row -->




            </div>
            <!--/Main Page Content -->



        </div>
        <!--/Main Content -->
    </div>
    <!--/Main -->


    <!-- Footer -->
    <footer class="hidden">
        <div id="footer-container">

            <div id="backtotop" class="button-wrap left-custom">
                <div class="icon-wrap parallax-wrap">
                    <div class="button-icon parallax-element">
                        <i class="fa fa-angle-up"></i>
                    </div>
                </div>
                <div class="button-text"><span data-hover="Voltar ao topo">Voltar ao topo</span></div>
            </div>

            <div class="socials-wrap">
                <div class="socials-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                <div class="socials-text">Siga a Sila Decor</div>
                <ul class="socials">
                    <li style="{{ isset($informations->facebook)!=""?'':'display:none;' }}" }}>
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->facebook }}" target="_blank">
                                <i class="fa fa-facebook-official" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>

                    <li style="{{ isset($informations->instagram)!=""?'':'display:none;' }}">
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->instagram }}" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>

                    <li style="{{ isset($informations->linkedin)!=""?'':'display:none;' }}">
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->linkedin }}" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>

                    <li style="{{ isset($informations->twitter)!=""?'':'display:none;' }}"><span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->twitter }}" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a></span>
                    </li>

                    <li style="{{ isset($informations->pinterest)!=""?'':'display:none;' }}">
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->pinterest }}" target="_blank">
                                <i class="fa fa-pinterest" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>
                </ul>
            </div>

        </div>
    </footer>
    <!--/Footer -->


</div>


@endsection




